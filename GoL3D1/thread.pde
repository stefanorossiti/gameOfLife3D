public class thVita extends Thread {
  private boolean running;
  private int wait;

  public thVita () {
    running = true;
    this.wait = round((1/csAgg.getValueF())*1000);
    this.start();
  }

  boolean isRunning(){
    return this.running;
  }
  
  public void setWait(int wait){
    this.wait = wait;
    //println(wait);
  }

  void run () {
    //apro il socket
    while (running) {
      try {
        //println("agg");
        getFate();
        Thread.sleep(wait); // 1/40 di secondo
        setFate();
      }
      catch(InterruptedException e) {
        running = false;
      }
    }
  }
}

