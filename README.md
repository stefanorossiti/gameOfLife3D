# Game Of life 3D

This is a 3D version of Conway's Game of Life.

Always wonder how Conway's game of life would develops in a 3D environnment? now you can!

- 3D interface (rotate, zoom, and explode)

- Settings GUI to change GOL3D rules run-time

- Play/Pause the simulation

- Color based on future cell state

It is a zero-player game, meaning that its evolution is determined by its initial state, requiring no further input.

One interacts with the Game of Life by creating an initial configuration and observing how it evolves.

In this version you can change the rules of the game and see the simulation in a 3D environment.

**Note: the demo following gif takes a while to load**

![gif](images/Gol3d.gif)

## Running the project

### Requirements

- [Java 7](https://www.oracle.com/java/technologies/javase/javase7-archive-downloads.html)

### Executable

The project is already compiled and ready to run. You can download the executable for windows and linux here: [releases](https://gitlab.com/stefanorossiti/gameOfLife3D/-/releases)

## Instructions

Start the project, you can move the camera with your mouse

![Scheme](images/3d.png)

You can change game rules and view settings

![Scheme](images/menu.png)

Rules settings translation:

If alive:

- dies with less than `{specified}` neighbours

- dies with more than `{specified}` neighbours

If dead:

- dies with more than `{specified}` neighbours

- dies with less than `{specified}` neighbours

## Development Requirements

This program run with [Processing 2.2.2](https://processing.org/). Processing is a **standalone** client.

You must use the right versions otherwise the progect wont work.

[Processing 2.2.1](https://processing.org/releases) (search for the correct archieved version)

### Libraries

These 2 libraries are needed to add more views to the GUIs.

- [G4P 3.5.4](https://sourceforge.net/projects/g4p/files/?source=navbar) (search the correct archieved version)
- [PFrame](https://github.com/shigeodayo/PFrame)

Extract both libraries in a separate folder into sketchbook libraries folder:

- Default libraries folder: `<user>\Documents\Processing\libraries`. If it doesn't exist, create it.

![alt text](images/libsPath.png)

You can now open processing and run the project.

### Alternative downloads

I did a backup of the dependencies in case the original links are down.

[Google Drive](https://drive.google.com/drive/folders/1ik1m29vXTRL1l6UIpJSqgC5Y5LQx7pbR?usp=sharing)

And the 2 libraries are in the repository itlself in the `Backups_libraries` folder.

### Run

Clone the repo, install both libraries and open with processing 2.2.1 the folder `GoL3D`, press play.

```bash
git clone https://gitlab.com/stefanorossiti/gameOfLife3D.git
```

## License

Released under [DBAD](https://www.dbad-license.org/) license.
